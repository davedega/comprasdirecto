package com.dega.moma.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dega.moma.R;

public class HomeView extends Fragment implements IHomeViewModel,
		OnClickListener {
	LinearLayout mView;
	IHomeViewModelListener mListener;
	Button rateUs, exhibitions, myLocation, ourStore;
	TextView warning;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (LinearLayout) inflater.inflate(R.layout.fragment_home,
				container, false);
		rateUs = (Button) mView.findViewById(R.id.rate_us_btn);
		exhibitions = (Button) mView.findViewById(R.id.exhibitions_btn);
		myLocation = (Button) mView.findViewById(R.id.my_location_btn);
		ourStore = (Button) mView.findViewById(R.id.our_store_btn);
		warning = (TextView) mView.findViewById(R.id.warning);
		rateUs.setOnClickListener(this);
		exhibitions.setOnClickListener(this);
		myLocation.setOnClickListener(this);
		ourStore.setOnClickListener(this);
		warning.setOnClickListener(this);

		return mView;
	}

	@Override
	public void setListener(IHomeViewModelListener listener) {
		mListener = listener;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.rate_us_btn:
			mListener.onReteUs();
			break;
		case R.id.exhibitions_btn:
			mListener.onExhibitions();
			break;
		case R.id.my_location_btn:
			mListener.onMyLocation();
			break;
		case R.id.our_store_btn:
			mListener.onOurStore();
			break;
		case R.id.warning:
			mListener.onTurnOnBluetooth();
			break;
		default:
			break;
		}

	}

	@Override
	public void showWarning() {
		warning.setVisibility(View.VISIBLE);
	}

	@Override
	public void hideWarning() {
		warning.setVisibility(View.GONE);

	}

}
