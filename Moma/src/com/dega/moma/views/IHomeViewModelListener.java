package com.dega.moma.views;

public interface IHomeViewModelListener {

	public void onReteUs();

	public void onExhibitions();

	public void onMyLocation();

	public void onOurStore();

	public void onTurnOnBluetooth();

}
