package com.dega.moma.views;

public interface IHomeViewModel {
	public void setListener(IHomeViewModelListener listener);

	public void showWarning();

	public void hideWarning();

}
