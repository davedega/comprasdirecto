package com.dega.moma.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.dega.moma.R;

public class TutorialView extends Fragment implements ITutorialViewModel,
		OnClickListener {
	ITutorialViewModelListener mListener;
	LinearLayout mView;
	ImageView tutorial;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (LinearLayout) inflater.inflate(R.layout.fragment_tutorial,
				container, false);
		tutorial = (ImageView) mView.findViewById(R.id.tuto);
		tutorial.setOnClickListener(this);
		return mView;
	}

	@Override
	public void setListener(ITutorialViewModelListener listener) {
		mListener = listener;
	}

	@Override
	public void onClick(View v) {
		mListener.onCloseT();

	}

}
