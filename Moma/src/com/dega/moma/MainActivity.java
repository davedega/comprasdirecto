package com.dega.moma;

import java.util.Set;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends BaseActionBarActivity {

	// iOS Developer webmaster_jairu
	// K2dj gauguinBeacon
	// TyHu diegoBeacon
	// tJMi salidaBeacon

	private final static int REQUEST_ENABLE_BT = 1;

	// Create a BroadcastReceiver for ACTION_FOUND

	public static void startActivity(Context ctx) {
		Intent intent = new Intent(ctx, MainActivity.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(intent);
	}

	private final BroadcastReceiver mReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			Log.i("Action: ", "" + action);
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				Log.i("DE BDROAD TRAIGO EL NOMBRE: ", "" + device.getName()
						+ " Y LA ADDRESS: " + device.getAddress());
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_main);
//		aja();

		Log.i("", "VERIFICANDO BLUETOOTH");
		BluetoothAdapter mBluetoothAdapter = BluetoothAdapter
				.getDefaultAdapter();

		if (mBluetoothAdapter == null) {
			Log.i("", "CEROS PAPA! ");

		} else {
			start(mBluetoothAdapter);
		}
	}



	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(mReceiver);
	}

	public void start(BluetoothAdapter mBluetoothAdapter) {
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT);
		}
		mBluetoothAdapter.startDiscovery();

		Set<BluetoothDevice> pairedDevices = mBluetoothAdapter
				.getBondedDevices();
		// If there are paired devices
		Log.i("SIZE: ", "" + pairedDevices.size());
		if (pairedDevices.size() > 0) {
			// Loop through paired devices
			for (BluetoothDevice device : pairedDevices) {
				// Add the name and address to an array adapter to show in a
				// ListView
				// mArrayAdapter.add(device.getName() + "\n" +
				// device.getAddress());

				Log.i("NAME: ",
						"" + device.getName() + "ADDRESS: "
								+ device.getAddress());
			}
		}
		// mBluetoothAdapter.startDiscovery();

		// Register the BroadcastReceiver
		IntentFilter filterDiscovery = new IntentFilter(
				BluetoothDevice.ACTION_FOUND);
		registerReceiver(mReceiver, filterDiscovery); // Don't forget to
														// unregister during
														// onDestro

	}

}
