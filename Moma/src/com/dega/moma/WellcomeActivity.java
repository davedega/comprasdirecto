package com.dega.moma;

import com.dega.moma.controllers.HomeController;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

public class WellcomeActivity extends BaseActionBarActivity {
	ImageView wellcome;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_wellcome);
		wellcome = (ImageView) findViewById(R.id.wellcome_iv);
		wellcome.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View arg0) {
				// MainActivity.startActivity(getApplicationContext());
				HomeController.startActivity(getApplicationContext());
			}
		});
		aja();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.wellcome, menu);
		return true;
	}
	
	
	public void aja() {
		int x = 1;
		int i = 1;
		while (x < 1000) {
			x = 2 * x;
			i = i + 1;
		}
		Log.e("AEGQERGSDRGTTTYTWRT", "value for i: " + i);

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

}
