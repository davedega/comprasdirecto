package com.dega.moma.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.dega.moma.BaseActionBarActivity;
import com.dega.moma.MOMA;
import com.dega.moma.R;
import com.dega.moma.views.IOurStoreViewModel;
import com.dega.moma.views.IOurStoreViewModelListener;

public class OurStoreController extends BaseActionBarActivity implements
		IOurStoreViewModelListener {
	IOurStoreViewModel viewModel;

	public static void startActivity(Context ctx) {
		Intent intent = new Intent(ctx, OurStoreController.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		// intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		ctx.startActivity(intent);

	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_our_store);
		viewModel = (IOurStoreViewModel) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_our_store);
		viewModel.setListener(this);
	}

	@Override
	public void onRateT() {

	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		MOMA.getInstance().setCurrentBeacon("free");
	}

}
