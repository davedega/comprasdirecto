package com.dega.moma.controllers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.widget.Toast;

import com.dega.moma.BaseActionBarActivity;
import com.dega.moma.MOMA;
import com.dega.moma.R;
import com.dega.moma.views.ExhibitionsView;
import com.dega.moma.views.IHomeViewModel;
import com.dega.moma.views.IHomeViewModelListener;
import com.dega.moma.views.MyLocationView;
import com.dega.moma.views.OurStoreView;
import com.dega.moma.views.RateUsView;

public class HomeController extends BaseActionBarActivity implements
		IHomeViewModelListener {

	IHomeViewModel viewModel;

	private final String I = HomeController.this.getClass().getName()
			.toString();
	private static final int REQUEST_ENABLE_BT = 1;
	private BluetoothAdapter myBluetoothAdapter;

	private final String SALIDA = "salidaBeacon";
	private final String DIEGO = "diegoBeacon";
	private final String GAUGIN = "gauguinBeacon";

	public static void startActivity(Context ctx) {
		Intent intent = new Intent(ctx, HomeController.class);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(intent);
	}

	private void startFragment(Fragment otro) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		fragmentManager.beginTransaction().replace(R.id.content_frame, otro)
				.commit();
	}

	final BroadcastReceiver bReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// When discovery finds a device
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				String newBeacon = device.getName();
				String currentBaacon = MOMA.getInstance().getCurrentBeacon();

				Log.i("newBeacon:", "" + newBeacon);
				Log.i("currentBaacon:", "" + currentBaacon);
				if (newBeacon != null) {
					if (newBeacon.equals(SALIDA)) {
						if (!currentBaacon.equals(SALIDA)) {

							OurStoreController
									.startActivity(getApplicationContext());
							MOMA.getInstance().setCurrentBeacon(SALIDA);

						}

					} else if (newBeacon.equals(DIEGO)) {
						if (!currentBaacon.equals(DIEGO)) {

							DiegoController
									.startActivity(getApplicationContext());
							MOMA.getInstance().setCurrentBeacon(DIEGO);
						}

					} else if (newBeacon.equals(GAUGIN)) {
						if (!currentBaacon.equals(GAUGIN)) {
							GauguinController
									.startActivity(getApplicationContext());
							MOMA.getInstance().setCurrentBeacon(GAUGIN);
						}

					}
				}

				myBluetoothAdapter.cancelDiscovery();
				myBluetoothAdapter.startDiscovery();

			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		getSupportActionBar().setDisplayShowTitleEnabled(false);
		viewModel = (IHomeViewModel) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_home);
		viewModel.setListener(this);
		startFragment(new MyLocationView());

	}

	@Override
	protected void onStart() {
		super.onStart();
		Log.i(I, "onStart");
//		turnOnBluetooth();
	}

	@Override
	protected void onResume() {
		Log.i(I, "onResume");
		super.onResume();
		turnOnBluetooth();

	}

	@Override
	public void onReteUs() {
		Log.i(I, "onReteUs");
		startFragment(new RateUsView());
	}

	@Override
	public void onExhibitions() {
		Log.i(I, "onExhibitions");
		startFragment(new ExhibitionsView());

	}

	@Override
	public void onMyLocation() {
		Log.i(I, "onMyLocation");
		startFragment(new MyLocationView());

	}

	@Override
	public void onOurStore() {
		Log.i(I, "onOurStore");
		startFragment(new OurStoreView());

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == REQUEST_ENABLE_BT) {
			if (myBluetoothAdapter.isEnabled()) {
				viewModel.hideWarning();
				find();
				TutorialController.startActivity(getApplicationContext());

			} else {
				viewModel.showWarning();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.e("unregisterReceiver", "unregisterReceiver");
		unregisterReceiver(bReceiver);
	}

	public void find() {
//		if (myBluetoothAdapter.isDiscovering()) {
//			Log.e("find()", "cancelDiscovery()");
//			myBluetoothAdapter.cancelDiscovery();
//		} else {
			Log.e("find()", "startDiscovery()");
			myBluetoothAdapter.startDiscovery();
			registerReceiver(bReceiver, new IntentFilter(
					BluetoothDevice.ACTION_FOUND));
//		}
	}

	@Override
	public void onTurnOnBluetooth() {
		if (!myBluetoothAdapter.isEnabled()) {
			Log.e("", "onTurnOnBluetooth()");
			Intent turnOnIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);
		} else {
			find();
			viewModel.hideWarning();
		}
	}

	public void turnOnBluetooth() {
		myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (myBluetoothAdapter == null) {
			viewModel.showWarning();
			Toast.makeText(getApplicationContext(),
					"Your device does not support Bluetooth", Toast.LENGTH_LONG)
					.show();
		} else {
			this.onTurnOnBluetooth();
		}

	}
}