package com.dega.moma;

import android.app.Application;
import android.content.Context;

public class MOMA extends Application {

	public static Context _ctx;
	private static MOMA sInstance;
	private String djha;

	@Override
	public void onCreate() {
		super.onCreate();
		MOMA._ctx = getApplicationContext();
		sInstance = this;
		djha = "free";
	}

	public static MOMA getInstance() {
		return sInstance;
	}

	public static Context getAppContext() {
		return _ctx;
	}

	public void setCurrentBeacon(String current) {
		djha = current;
	}

	public String getCurrentBeacon() {
		return djha;
	}

}
