package com.dega.corona.views;

public interface IScannerVM {

	public void setListener(IScannerVMListener listener);

	public void showWarning();

	public void hideWarning();

}
