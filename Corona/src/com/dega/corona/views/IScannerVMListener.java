package com.dega.corona.views;

public interface IScannerVMListener {
	
	public void onTurnOnBluetooth();

	public void onFridgeFound();
}
