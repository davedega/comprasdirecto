package com.dega.corona.views;

import com.dega.corona.modelo.Fridge;

public interface IEncuestaVM {
	public void setListener(IEncuestaVMListener listener);

	public void setData(Fridge fridge);

	public String[] getResult();

	public void switchQ1();

	public void switchQ2();

	public void switchQ3();

	public String getResult1();

	public String getResult2();

	public String getResult3();

}
