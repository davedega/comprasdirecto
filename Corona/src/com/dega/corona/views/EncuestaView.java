package com.dega.corona.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.dega.corona.R;
import com.dega.corona.modelo.Fridge;

public class EncuestaView extends Fragment implements IEncuestaVM,
		OnClickListener {
	ScrollView mView;
	IEncuestaVMListener mListener;
	ImageView r1, r2, r3;
	Button enviarBtn;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (ScrollView) inflater.inflate(R.layout.fragment_encuesta,
				container, false);

		r1 = (ImageView) mView.findViewById(R.id.question1);
		r2 = (ImageView) mView.findViewById(R.id.question2);
		r3 = (ImageView) mView.findViewById(R.id.question3);
		enviarBtn= (Button) mView.findViewById(R.id.guardar_btn);
		r1.setOnClickListener(this);
		r2.setOnClickListener(this);
		r3.setOnClickListener(this);
		enviarBtn.setOnClickListener(this);
		init();
		return mView;
	}

	@Override
	public void setListener(IEncuestaVMListener listener) {
		mListener = listener;
	}

	@Override
	public void setData(Fridge fridge) {
		// TODO Auto-generated method stub

	}

	@Override
	public String[] getResult() {
		String[] arre = new String[3];
		String res1 = null;
		String res2 = null;
		String res3 = null;

		Integer tag1, tag2, tag3;
		tag1 = (Integer) r1.getTag();
		tag2 = (Integer) r2.getTag();
		tag3 = (Integer) r3.getTag();

		if (tag1 == 1) {
			res1 = "SI";

		} else if (tag1 == 2) {
			res1 = "NO";
		}

		if (tag2 == 1) {
			res2 = "SI";

		} else if (tag2 == 2) {
			res2 = "NO";

		}

		if (tag3 == 1) {
			res3 = "SI";

		} else if (tag3 == 2) {
			res3 = "NO";

		}
		arre[0] = res1;
		arre[1] = res2;
		arre[2] = res3;

		return null;
	}

	@Override
	public void switchQ1() {
		int tag1 = (Integer) r1.getTag();

		if (tag1 == 0) {
			r1.setTag(1);
			r1.setBackgroundResource(R.drawable.tick);
		} else if (tag1 == 1) {
			r1.setTag(2);
			r1.setBackgroundResource(R.drawable.cross);

		} else if (tag1 == 2) {
			r1.setTag(1);
			r1.setBackgroundResource(R.drawable.tick);

		}
	}

	@Override
	public void switchQ2() {
		int tag2 = (Integer) r2.getTag();

		if (tag2 == 0) {
			r2.setTag(1);
			r2.setBackgroundResource(R.drawable.tick);
		} else if (tag2 == 1) {
			r2.setTag(2);
			r2.setBackgroundResource(R.drawable.cross);

		} else if (tag2 == 2) {
			r2.setTag(1);
			r2.setBackgroundResource(R.drawable.tick);

		}
	}

	@Override
	public void switchQ3() {
		int tag3 = (Integer) r3.getTag();

		if (tag3 == 0) {
			r3.setTag(1);
			r3.setBackgroundResource(R.drawable.tick);
		} else if (tag3 == 1) {
			r3.setTag(2);
			r3.setBackgroundResource(R.drawable.cross);

		} else if (tag3 == 2) {
			r3.setTag(1);
			r3.setBackgroundResource(R.drawable.tick);

		}
	}

	public void init() {

		r1.setBackgroundResource(R.drawable.blank);
		r2.setBackgroundResource(R.drawable.blank);
		r3.setBackgroundResource(R.drawable.blank);

		r1.setTag(0);
		r2.setTag(0);
		r3.setTag(0);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.question1:
			mListener.onQuestion1T();
			break;

		case R.id.question2:
			mListener.onQuestion2T();

			break;
		case R.id.question3:
			mListener.onQuestion3T();

			break;
			
		case R.id.guardar_btn:
			mListener.onEnviarT();

			break;

		default:
			break;
		}

	}

	@Override
	public String getResult1() {
		int tag1 = (Integer) r1.getTag();

		if (tag1 == 1) {
			return "SI";

		} else if (tag1 == 2) {
			return "NO";

		}
		return null;
	}

	@Override
	public String getResult2() {
		int tag2 = (Integer) r2.getTag();

		if (tag2 == 1) {
			return "SI";

		} else if (tag2 == 2) {
			return "NO";

		}
		return null;
	}

	@Override
	public String getResult3() {
		int tag3 = (Integer) r3.getTag();

		if (tag3 == 1) {
			return "SI";

		} else if (tag3 == 2) {
			return "NO";

		}
		return null;
	}

}
