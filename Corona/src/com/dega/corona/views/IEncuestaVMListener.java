package com.dega.corona.views;

public interface IEncuestaVMListener {

	public void onQuestion1T();

	public void onQuestion2T();

	public void onQuestion3T();

	public void onEnviarT();

}
