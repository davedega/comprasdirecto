package com.dega.corona.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dega.corona.R;

public class ScannerView extends Fragment implements IScannerVM,
		OnClickListener {
	LinearLayout mView;
	IScannerVMListener mListener;
	TextView warning;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (LinearLayout) inflater.inflate(R.layout.fragment_scanner,
				container, false);
		warning = (TextView) mView.findViewById(R.id.warning);
		warning.setOnClickListener(this);

		return mView;
	}

	@Override
	public void setListener(IScannerVMListener listener) {
		mListener = listener;
	}

	@Override
	public void showWarning() {
		warning.setVisibility(View.VISIBLE);

	}

	@Override
	public void hideWarning() {
		warning.setVisibility(View.GONE);

	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.warning:
			mListener.onTurnOnBluetooth();
			break;
		default:
			break;
		}

	}
}
