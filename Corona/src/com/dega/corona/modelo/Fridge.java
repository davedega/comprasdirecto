package com.dega.corona.modelo;

public class Fridge {

	private String idBeacon;
	private String nameBeacon;

	public String getIdBeacon() {
		return idBeacon;
	}

	public void setIdBeacon(String idBeacon) {
		this.idBeacon = idBeacon;
	}

	public String getNameBeacon() {
		return nameBeacon;
	}

	public void setNameBeacon(String nameBeacon) {
		this.nameBeacon = nameBeacon;
	}

}
