package com.dega.corona;

import android.app.Application;
import android.content.Context;

public class CORONA extends Application {

	public static Context _ctx;
	private static CORONA sInstance;
	private boolean showing;

	@Override
	public void onCreate() {
		super.onCreate();
		CORONA._ctx = getApplicationContext();
		sInstance = this;
		showing = true;
	}

	public static CORONA getInstance() {
		return sInstance;
	}

	public static Context getAppContext() {
		return _ctx;
	}

	public void switchLock(boolean signal) {
		showing = signal;
	}

	public boolean getPermission() {
		return showing;
	}

}
