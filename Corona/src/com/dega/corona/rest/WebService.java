package com.dega.corona.rest;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

import com.dega.corona.modelo.Constantes;
import com.google.common.io.LineReader;

public class WebService {
	public static String doGet(String url) {
		String respuesta = null;

		HttpClient httpClient = MySSLSocketFactory.getNewHttpClient();
		HttpGet httpGet = new HttpGet(url);
//		httpPost.setHeader("Content-type", "application/json");

		Log.i("doGet", "url > " + url);

		try {
			

			HttpResponse response = httpClient.execute(httpGet);
			LineReader reader = new LineReader(new InputStreamReader(response
					.getEntity().getContent()));

			respuesta = reader.readLine();
			Log.i("doGet", "out >" + respuesta);
			// if (respuesta.contains("<!DOCTYPE")) {
			// throw new InvalidUserException("Invalid user");
			// }

		} catch (ClientProtocolException e) {
			throw new RuntimeException("Error http", e);
		} catch (IOException e) {
			Log.i("IOException", "" + e);
			// throw new InternetConnectionException(e.toString(), e);
		}
		return respuesta;
	}
	public static String doPost(String message, String url) {
		String respuesta = null;

		HttpClient httpClient = MySSLSocketFactory.getNewHttpClient();
		HttpPost httpPost = new HttpPost(url);
		httpPost.setHeader("Content-type", "application/json");

		Log.i("doPost", "url > " + url);

		try {
			if (message != null) {
				Log.i("doPost", "in > " + message.toString());
				HttpEntity httpEntity;
				StringEntity stringEntity = new StringEntity(message);
				// stringEntity.setContentEncoding(new BasicHeader(
				// HTTP.CONTENT_TYPE, "application/json"));
				httpEntity = stringEntity;
				// httpPost.setHeader("Authorization", token);
				httpPost.setEntity(httpEntity);
			}

			HttpResponse response = httpClient.execute(httpPost);
			LineReader reader = new LineReader(new InputStreamReader(response
					.getEntity().getContent()));

			respuesta = reader.readLine();
			Log.i("doPost", "out >" + respuesta);
			// if (respuesta.contains("<!DOCTYPE")) {
			// throw new InvalidUserException("Invalid user");
			// }

		} catch (ClientProtocolException e) {
			throw new RuntimeException("Error http", e);
		} catch (IOException e) {
			Log.i("IOException", "" + e);
			// throw new InternetConnectionException(e.toString(), e);
		}
		return respuesta;
	}

	public static void enviarResultados(String res1, String res2, String res3) {
		// Respuesta laChida = null;
		String url = Constantes.ENVIAR_RESPUESTAS;
		String mensaje = Serializer.serializeRespuesta(res1, res2, res3);
		String response = doPost(mensaje, url);
		// try {
		// JSONObject object = new JSONObject(response);
		// JSONObject jsonall = (JSONObject) object.get("datos");
		// JSONObject jsonRes = (JSONObject) jsonall.get("Respuesta");
		// Gson gson = new GsonBuilder().create();
		// laChida = gson.fromJson(jsonRes.toString(), Respuesta.class);
		// if (laChida.Monto != null) {
		// AppPreferences.saveMonto(laChida.Monto);
		// }
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }

		// return laChida.Idestatus;
	}

	public static void enviarResultados2(String res1, String res2, String res3) {

		HttpClient httpclient = MySSLSocketFactory.getNewHttpClient();
		try {
			URI url;

			url = new URI(Constantes.ENVIAR_RESPUESTAS);
			Log.i("login", "in >" + url);

			HttpPost httppost = new HttpPost(url);
			List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
			nameValuePairs.add(new BasicNameValuePair("respuesta", res1 + ","
					+ res2 + "," + res3));

			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			LineReader reader = new LineReader(new InputStreamReader(response
					.getEntity().getContent()));
			String respuesta = reader.readLine();
			Log.i("login", "out >" + respuesta);
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void enviarResultados3(String res1, String res2, String res3) {

		String mURL = Constantes.ENVIAR_RESPUESTAS;
		Log.i("enviarResultados3", "" + mURL);
		HttpClient httpClient = new DefaultHttpClient();
		HttpPost httpPost = new HttpPost(mURL);
		List<NameValuePair> nameValuePair = new ArrayList<NameValuePair>(2);
		nameValuePair.add(new BasicNameValuePair("respuesta", res1 + ","
				+ res2 + "," + res3));
		
		

		try {
			httpPost.setEntity(new UrlEncodedFormEntity(nameValuePair));
			HttpResponse response = httpClient.execute(httpPost);
			LineReader reader = new LineReader(new InputStreamReader(response
					.getEntity().getContent()));
			String respuesta = reader.readLine();
			Log.i("saveAndPublish", "out >" + respuesta);
		} catch (ClientProtocolException e) {
			throw new RuntimeException("Error http", e);
		} catch (IOException e) {
			Log.i("IOException", "" + e);
			// throw new InternetConnectionException(
			//	ctx.getString(R.string.conection_problem), e);

		}

	}
	
	
	
	public static void enviarResultados4(String res1, String res2, String res3) {
		// Respuesta laChida = null;
		String mensaje = Serializer.serializeRespuesta(res1, res2, res3);

		String url = Constantes.ENVIAR_RESPUESTAS+"?respuestas="+mensaje;
		String response = doGet(url);
		// try {
		// JSONObject object = new JSONObject(response);
		// JSONObject jsonall = (JSONObject) object.get("datos");
		// JSONObject jsonRes = (JSONObject) jsonall.get("Respuesta");
		// Gson gson = new GsonBuilder().create();
		// laChida = gson.fromJson(jsonRes.toString(), Respuesta.class);
		// if (laChida.Monto != null) {
		// AppPreferences.saveMonto(laChida.Monto);
		// }
		// } catch (JSONException e) {
		// e.printStackTrace();
		// }

		// return laChida.Idestatus;
	}
}
