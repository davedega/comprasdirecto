package com.dega.corona;

import java.util.Timer;
import java.util.TimerTask;

import com.dega.corona.controllers.ScannerController;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;

public class SplashScreen extends ActionBarActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.fragment_splash_screen);
		getActionBar().hide();

		TimerTask task = new TimerTask() {
			@Override
			public void run() {
				ScannerController.startActivity(getApplicationContext());
			}
		};
		Timer timer = new Timer();
		timer.schedule(task, 3000);
	}

}
