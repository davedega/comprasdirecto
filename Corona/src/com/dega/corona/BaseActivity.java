package com.dega.corona;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface.OnClickListener;
import android.content.pm.ActivityInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

public class BaseActivity extends ActionBarActivity {
	static ProgressDialog dialogo;
	private final static int CONNECTION_FAILURE_RESOLUTION_REQUEST = 9000;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
	}

	/**
	 * Este m�todo sirve para mostrar un WaitingIndicator mientras se ejecuta
	 * una tarea en segundo plano, al termianr de ejecutarse se ejecuta una
	 * segunda tarea.
	 * 
	 * @param _ctx
	 *            El contexto desde dodne se desean ejecutar las tareas
	 * @param task
	 *            La tarea que se ejecuta en segundo plano
	 * @param completion
	 *            La segunda tarea que se ejecuta al terminar la primer
	 * @author davedega
	 **/

	public void showWaitDialogWhileExecuting(String msg, final Runnable task,
			final Runnable completion) {
		showWaitDialogWhileExecutingg(msg, task, completion);
	}

	public void showWaitDialogWhileExecuting(final Runnable task,
			final Runnable completion) {
		showWaitDialogWhileExecutingg("", task, completion);
	}

	protected void showWaitDialogWhileExecutingg(String msg,
			final Runnable task, final Runnable completion) {

		dialogo = new ProgressDialog(this);
		dialogo.setMessage(msg);
		dialogo.setCanceledOnTouchOutside(false);
		dialogo.setCancelable(false);

		AsyncTask<Void, Void, Throwable> asyncTask = new AsyncTask<Void, Void, Throwable>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
				dialogo.show();
			}

			@Override
			protected Throwable doInBackground(Void... params) {
				try {
					if (task != null)
						task.run();
				} catch (Exception e) {
					dialogo.dismiss();
					Log.e("doInBackground", "Excpetion" + e);
					return e;
				}
				return null;
			}

			@Override
			protected void onPostExecute(Throwable result) {
				if (result != null) {
					dialogo.dismiss();
					handleException(getApplicationContext(), result);
				} else if (completion != null) {
					dialogo.dismiss();
					completion.run();

				} else
					dialogo.dismiss();
			}
		};
		asyncTask.execute();
	}

	public void executeInBackground(Runnable task, Runnable completion) {
		executeInBackgroundd(task, completion);
	}

	/**
	 * Ejecuta una tarea en segundo plano y al terminar ejecuta la segunda tarea
	 ***/
	public void executeInBackgroundd(final Runnable task,
			final Runnable completion) {
		AsyncTask<Void, Void, Throwable> asyncTask = new AsyncTask<Void, Void, Throwable>() {
			@Override
			protected void onPreExecute() {
				super.onPreExecute();
			}

			@Override
			protected Throwable doInBackground(Void... params) {
				try {
					if (task != null)
						task.run();
				} catch (Exception e) {
					Log.e("executeInBackground", "Excpetion" + e);
					return e;
				}
				return null;
			}

			@Override
			protected void onPostExecute(Throwable result) {
				if (result != null) {
					handleExceptionInBackground(result);
				} else if (completion != null) {
					completion.run();
				}
			}
		};
		asyncTask.execute();
	}

	/**
	 * Cuando se genera una excepcion se llama este metodo para manejarla
	 */
	public void handleExceptionInBackground(Throwable caught) {
		Log.e("handleExceptionInBackground()", "causa: "
				+ caught.getMessage().toString());
	}

	/**
	 * Cuando se genera una excepcion se llama este metodo para manejarla
	 */
	public void handleException(Context ctx, final Throwable caught) {
		Log.e("handleException()", "causa: " + caught.getMessage().toString());

	}

	public static void showMessage(Context ctx, String title, String message) {
		Log.i("showMessage", "message: " + message);
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton("OK", null);
		builder.show();
	}

	public static void showMessage(Context ctx, String message) {
		Log.i("showMessage", "message: " + message);
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle("!");
		builder.setMessage(message);
		builder.setPositiveButton("OK", null);
		builder.show();
	}

	public static void showMessage(Context ctx, String title, String message,
			OnClickListener listener) {
		AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(ctx.getString(R.string.accept), listener);
		builder.setNegativeButton(ctx.getString(R.string.cancel), null);
		builder.show();
	}

	protected void showToast(String mensaje) {
		Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT)
				.show();
	}

	public static void showMessage(Context _ctx, String title, String message,
			OnClickListener onClickListener, OnClickListener onClickListener2) {
		AlertDialog.Builder builder = new AlertDialog.Builder(_ctx);
		builder.setTitle(title);
		builder.setMessage(message);
		builder.setPositiveButton(_ctx.getString(R.string.accept),
				onClickListener);
		builder.setNegativeButton(_ctx.getString(R.string.cancel),
				onClickListener2);
		builder.show();
	}




	


}
