package com.dega.corona.controllers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.widget.Toast;

import com.dega.corona.CORONA;
import com.dega.corona.R;
import com.dega.corona.views.IScannerVM;
import com.dega.corona.views.IScannerVMListener;

public class ScannerController extends ActionBarActivity implements
		IScannerVMListener {

	IScannerVM viewModel;
	private static final int REQUEST_ENABLE_BT = 1;
	private BluetoothAdapter myBluetoothAdapter;
	
	private final String SALIDA = "salidaBeacon";

	public static void startActivity(Context ctx) {
		Intent ac = new Intent(ctx, ScannerController.class);
		ac.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(ac);
	}

	final BroadcastReceiver reciverFridge = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			BluetoothDevice device = intent
					.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
			String newBeacon = device.getName();
			// String currentBaacon = MOMA.getInstance().getCurrentBeacon();

			Log.i("newBeacon:", "" + newBeacon);
			// Log.i("currentBaacon:", "" + currentBaacon);
			if (newBeacon.equals(SALIDA)) {

				if (CORONA.getInstance().getPermission()) {
					CORONA.getInstance().switchLock(false);
					EncuestaController.startActivity(getApplicationContext(),
							"", "");

				}
			}

			myBluetoothAdapter.cancelDiscovery();
			myBluetoothAdapter.startDiscovery();

		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.scanner_activity);
		viewModel = (IScannerVM) getSupportFragmentManager().findFragmentById(
				R.id.fragment_scanner);
		viewModel.setListener(this);
		getActionBar().hide();
	}

	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
	}

	@Override
	protected void onResume() {
		super.onResume();
		turnOnBluetooth();

	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
	}

	@Override
	public void onFridgeFound() {
		// TODO Auto-generated method stub

	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == REQUEST_ENABLE_BT) {
			if (myBluetoothAdapter.isEnabled()) {
				viewModel.hideWarning();
				find();
//				TutorialController.startActivity(getApplicationContext());

			} else {
				viewModel.showWarning();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.e("unregisterReceiver", "unregisterReceiver");
		unregisterReceiver(reciverFridge);
	}
	
	
	public void turnOnBluetooth() {
		myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (myBluetoothAdapter == null) {
			viewModel.showWarning();
			Toast.makeText(getApplicationContext(),
					"Your device does not support Bluetooth", Toast.LENGTH_LONG)
					.show();
		} else {
			this.onTurnOnBluetooth();
		}

	}

	@Override
	public void onTurnOnBluetooth() {
		if (!myBluetoothAdapter.isEnabled()) {
			Log.e("", "onTurnOnBluetooth()");
			Intent turnOnIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);
		} else {
			find();
			viewModel.hideWarning();
		}
	}
	
	public void find() {
//		if (myBluetoothAdapter.isDiscovering()) {
//			Log.e("find()", "cancelDiscovery()");
//			myBluetoothAdapter.cancelDiscovery();
//		} else {
			Log.e("find()", "startDiscovery()");
			myBluetoothAdapter.startDiscovery();
			registerReceiver(reciverFridge, new IntentFilter(
					BluetoothDevice.ACTION_FOUND));
//		}
	}


}
