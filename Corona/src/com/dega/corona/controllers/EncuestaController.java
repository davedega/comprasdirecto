package com.dega.corona.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.dega.corona.BaseActivity;
import com.dega.corona.CORONA;
import com.dega.corona.R;
import com.dega.corona.modelo.Fridge;
import com.dega.corona.rest.WebService;
import com.dega.corona.views.IEncuestaVM;
import com.dega.corona.views.IEncuestaVMListener;

public class EncuestaController extends BaseActivity implements
		IEncuestaVMListener {
	IEncuestaVM viewModel;

	public static void startActivity(Context ctx, String idBeacon,
			String nameBeacon) {

		Intent in = new Intent(ctx, EncuestaController.class);
		in.putExtra("idBeacon", idBeacon);
		in.putExtra("nameBeacon", nameBeacon);
		in.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(in);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_encuesta);
		viewModel = (IEncuestaVM) getSupportFragmentManager().findFragmentById(
				R.id.fragment_encuesta);
		viewModel.setListener(this);
		getActionBar().hide();
		Fridge mFridge = new Fridge();
		mFridge.setIdBeacon(getIntent().getExtras().getString("idBeacon"));
		mFridge.setNameBeacon(getIntent().getExtras().getString("nameBeacon"));
		viewModel.setData(mFridge);
	}

	@Override
	public void onEnviarT() {

		showWaitDialogWhileExecuting(getString(R.string.enviando_respuesta),
				new Runnable() {

					@Override
					public void run() {
						WebService.enviarResultados4(viewModel.getResult1(),
								viewModel.getResult2(), viewModel.getResult3());

					}
				}, new Runnable() {

					@Override
					public void run() {
						finish();
					}
				});

	}

	@Override
	public void onQuestion1T() {
		viewModel.switchQ1();

	}

	@Override
	public void onQuestion2T() {
		viewModel.switchQ2();

	}

	@Override
	public void onQuestion3T() {
		viewModel.switchQ3();

	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		CORONA.getInstance().switchLock(true);

	}

}
