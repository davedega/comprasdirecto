package com.dega.axxa.views;

public interface IHomeViewModel {

	public void setListener(IHomeListener listener);

	public void showWarning();

	public void hideWarning();

}
