package com.dega.axxa.views;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RelativeLayout;

import com.dega.axxa.R;

public class WelcomeView extends Fragment implements IWelcomeViewModel,
		OnClickListener {
	RelativeLayout mView;
	IWelcomeListener mListener;
	Button tiendas, emergencia, cotiza, tusPolizas, meGusta;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (RelativeLayout) inflater.inflate(R.layout.fragment_welcome,
				container, false);
		tiendas = (Button) mView.findViewById(R.id.tiendas);
		emergencia = (Button) mView.findViewById(R.id.emergencia);
		cotiza = (Button) mView.findViewById(R.id.cotiza);
		tusPolizas = (Button) mView.findViewById(R.id.tus_polizas);
		meGusta = (Button) mView.findViewById(R.id.me_gusta_btn);
		tiendas.setOnClickListener(this);
		emergencia.setOnClickListener(this);
		cotiza.setOnClickListener(this);
		tusPolizas.setOnClickListener(this);

		return mView;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tiendas:
			mListener.onTiendasT();
			break;
		case R.id.emergencia:
			mListener.onEmergencias();
			;
			break;
		case R.id.cotiza:
			mListener.onCotiza();
			;
			break;
		case R.id.tus_polizas:
			mListener.onTusPolizasT();

			break;

		case R.id.me_gusta_btn:
			mListener.onMeGustaT();
			break;

		default:
			break;
		}

	}

	@Override
	public void setListener(IWelcomeListener listener) {
		mListener = listener;
	}

}
