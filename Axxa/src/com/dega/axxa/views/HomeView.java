package com.dega.axxa.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dega.axxa.BaseFragment;
import com.dega.axxa.R;

public class HomeView extends BaseFragment implements IHomeViewModel,
		OnClickListener {
	IHomeListener mListener;
	LinearLayout mView;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		mView = (LinearLayout) inflater.inflate(R.layout.fragment_home,
				container, false);

		return mView;
	}

	@Override
	public void setListener(IHomeListener mListener) {
		this.mListener = mListener;
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.warning:
			mListener.onTurnOnBluetooth();
			break;
		default:
			break;
		}
	}

	@Override
	public void showWarning() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hideWarning() {
		// TODO Auto-generated method stub

	}

}
