package com.dega.axxa.views;

public interface IWelcomeListener {

	public void onMeGustaT();

	public void onTiendasT();

	public void onEmergencias();

	public void onCotiza();

	public void onTusPolizasT();
}
