package com.dega.axxa.views;

public interface IHomeListener {

	public void onTiendaT();

	public void onCotizaT();

	public void onEmergenciasT();

	public void onPolizasT();

	public void onTurnOnBluetooth();

}
