package com.dega.axxa;

import android.app.Application;
import android.content.Context;

public class AXXA extends Application {

	public static Context _ctx;
	private static AXXA sInstance;
	private String djha;

	@Override
	public void onCreate() {
		super.onCreate();
		AXXA._ctx = getApplicationContext();
		sInstance = this;
		djha = "free";
	}

	public static AXXA getInstance() {
		return sInstance;
	}

	public static Context getAppContext() {
		return _ctx;
	}

	public void setCurrentBeacon(String current) {
		djha = current;
	}

	public String getCurrentBeacon() {
		return djha;
	}

}
