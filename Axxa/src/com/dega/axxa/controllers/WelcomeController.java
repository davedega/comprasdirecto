package com.dega.axxa.controllers;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.dega.axxa.AXXA;
import com.dega.axxa.BaseActivity;
import com.dega.axxa.R;
import com.dega.axxa.views.IWelcomeListener;
import com.dega.axxa.views.IWelcomeViewModel;

public class WelcomeController extends BaseActivity implements IWelcomeListener {

	IWelcomeViewModel viewModel;

	public static void startActivity(Context ctx) {
		Intent welcome = new Intent(ctx, WelcomeController.class);
		welcome.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		ctx.startActivity(welcome);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_welcome);
		getSupportActionBar().hide();
		viewModel = (IWelcomeViewModel) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_welcome);
		viewModel.setListener(this);
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		AXXA.getInstance().setCurrentBeacon("free");
	}

	@Override
	public void onMeGustaT() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTiendasT() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEmergencias() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCotiza() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onTusPolizasT() {
		// TODO Auto-generated method stub

	}

}
