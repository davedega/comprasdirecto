package com.dega.axxa.controllers;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.dega.axxa.AXXA;
import com.dega.axxa.BaseActivity;
import com.dega.axxa.R;
import com.dega.axxa.views.IHomeListener;
import com.dega.axxa.views.IHomeViewModel;

public class HomeController extends BaseActivity implements IHomeListener {

	// tJMi salidaBeacon

	IHomeViewModel viewModel;

	private static final int REQUEST_ENABLE_BT = 1;
	private BluetoothAdapter myBluetoothAdapter;
	private final String SALIDA = "salidaBeacon";

	final BroadcastReceiver bReceiver = new BroadcastReceiver() {
		public void onReceive(Context context, Intent intent) {
			String action = intent.getAction();
			// When discovery finds a device
			if (BluetoothDevice.ACTION_FOUND.equals(action)) {
				BluetoothDevice device = intent
						.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
				String newBeacon = device.getName();
				String currentBaacon = AXXA.getInstance().getCurrentBeacon();

				Log.i("newBeacon:", "" + newBeacon);
				Log.i("currentBaacon:", "" + currentBaacon);
				if (newBeacon != null) {
					if (newBeacon.equals(SALIDA)) {
						if (!currentBaacon.equals(SALIDA)) {
							WelcomeController
									.startActivity(getApplicationContext());
							AXXA.getInstance().setCurrentBeacon(SALIDA);
						}
					}
				}

				myBluetoothAdapter.cancelDiscovery();
				myBluetoothAdapter.startDiscovery();

			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		getSupportActionBar().hide();
		viewModel = (IHomeViewModel) getSupportFragmentManager()
				.findFragmentById(R.id.fragment_home);
		viewModel.setListener(this);
	}

	@Override
	protected void onResume() {
		super.onResume();
		turnOnBluetooth();

	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		// TODO Auto-generated method stub
		if (requestCode == REQUEST_ENABLE_BT) {
			if (myBluetoothAdapter.isEnabled()) {
				viewModel.hideWarning();
				find();
				// TutorialController.startActivity(getApplicationContext());

			} else {
				viewModel.showWarning();
			}
		}
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		unregisterReceiver(bReceiver);

	}

	public void find() {
		// if (myBluetoothAdapter.isDiscovering()) {
		// Log.e("find()", "cancelDiscovery()");
		// myBluetoothAdapter.cancelDiscovery();
		// } else {
		Log.e("find()", "startDiscovery()");
		myBluetoothAdapter.startDiscovery();
		registerReceiver(bReceiver, new IntentFilter(
				BluetoothDevice.ACTION_FOUND));
		// }
	}

	@Override
	public void onTurnOnBluetooth() {
		if (!myBluetoothAdapter.isEnabled()) {
			Log.e("", "onTurnOnBluetooth()");
			Intent turnOnIntent = new Intent(
					BluetoothAdapter.ACTION_REQUEST_ENABLE);
			startActivityForResult(turnOnIntent, REQUEST_ENABLE_BT);
		} else {
			find();
			viewModel.hideWarning();
		}
	}

	public void turnOnBluetooth() {
		myBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
		if (myBluetoothAdapter == null) {
			viewModel.showWarning();
			Toast.makeText(getApplicationContext(),
					"Your device does not support Bluetooth", Toast.LENGTH_LONG)
					.show();
		} else {
			this.onTurnOnBluetooth();
		}

	}

	@Override
	public void onTiendaT() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onCotizaT() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onEmergenciasT() {
		// TODO Auto-generated method stub

	}

	@Override
	public void onPolizasT() {
		// TODO Auto-generated method stub

	}

}
